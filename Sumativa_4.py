placa = ""
while True:
    placa = input("Ingrese la placa: ")
    if len(placa) != 5 or not placa.isdigit():
        print("Placa no válida")
    else:
        break
primer_digito = placa[0]
tipo_vehiculo = "Desconocido"
if primer_digito == "1":
    tipo_vehiculo = "Toyota"
elif primer_digito == "2":
    tipo_vehiculo = "Nissan"
print("La placa es de un vehículo " + tipo_vehiculo)
